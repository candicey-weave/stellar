plugins {
    kotlin("jvm") version "1.9.23"
    `kotlin-dsl`
    `maven-publish`
}

val projectName: String by project
val projectGroup: String by project
val projectVersion: String by project
val gitlabProjectId: String by project

group = projectGroup
version = projectVersion

repositories {
    mavenCentral()
}

dependencies {
    // OW2 ASM
    implementation(libs.asm)
    implementation(libs.asmCommons)

    // Kotlinx.serialization JSON library
    implementation(libs.kxSerJSON)

    testImplementation(kotlin("test"))
}

tasks.test {
    useJUnitPlatform()
}

kotlin {
    jvmToolchain(8)
}

java {
    withSourcesJar()
    withJavadocJar()
}

publishing {
    repositories {
        maven {
            url = uri("https://gitlab.com/api/v4/projects/$gitlabProjectId/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                name = "Private-Token"
                value = findProperty("gitLabPrivateToken") as String?
            }
            authentication {
                create("header", HttpHeaderAuthentication::class)
            }
        }
    }
}

gradlePlugin {
    plugins {
        create("stellar") {
            id = projectGroup
            displayName = projectName
            description = "A plugin for utilising Zenith Utilities"
            implementationClass = "$projectGroup.StellarMain"
        }
    }
}