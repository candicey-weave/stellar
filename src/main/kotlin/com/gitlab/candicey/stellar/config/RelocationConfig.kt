package com.gitlab.candicey.stellar.config

import org.gradle.api.Task
import java.io.File
import java.util.jar.JarEntry
import java.util.jar.JarFile
import java.util.jar.JarOutputStream

typealias RelocationApplier = Task.(originalJar: JarFile, newJar: JarOutputStream, entry: JarEntry, writeEntryToFile: (file: JarFile, outStream: JarOutputStream, entry: JarEntry, entryName: String) -> Unit) -> Boolean

typealias ShadowJarTransformer = (file: File) -> File

data class RelocationConfig(
    val compileShadowJars: MutableMap<String, ShadowJarTransformer?> = mutableMapOf(),
    val compileBundledJars: MutableList<File> = mutableListOf(),
    val relocationShadowJars: MutableMap<String, ShadowJarTransformer?> = mutableMapOf(),
    val relocationBundledJars: MutableList<File> = mutableListOf(),
    val classNameReplaceList: MutableMap<String, String> = mutableMapOf(),
    val stringReplaceList: MutableMap<String, String> = mutableMapOf(),
    val extraRelocationAppliers: MutableList<RelocationApplier> = mutableListOf(),
)

fun MutableMap<String, ShadowJarTransformer?>.add(jar: String) {
    this[jar] = null
}