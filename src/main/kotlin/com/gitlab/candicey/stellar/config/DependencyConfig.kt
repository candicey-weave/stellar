package com.gitlab.candicey.stellar.config

import com.gitlab.candicey.stellar.dependency.Dependency

data class DependencyConfig(
    val dependencies: MutableList<Dependency> = mutableListOf(),
)