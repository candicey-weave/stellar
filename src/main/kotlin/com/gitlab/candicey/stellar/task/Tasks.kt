package com.gitlab.candicey.stellar.task

import com.gitlab.candicey.stellar.STELLA_TASK_GROUP
import com.gitlab.candicey.stellar.config.RelocationConfig
import com.gitlab.candicey.stellar.ext
import org.gradle.api.Project
import org.gradle.api.file.DuplicatesStrategy
import org.gradle.api.tasks.bundling.Jar
import org.gradle.kotlin.dsl.get
import org.gradle.kotlin.dsl.getByName
import org.objectweb.asm.ClassReader
import org.objectweb.asm.ClassWriter
import org.objectweb.asm.commons.ClassRemapper
import org.objectweb.asm.commons.Remapper
import org.objectweb.asm.tree.ClassNode
import org.objectweb.asm.tree.LdcInsnNode
import org.objectweb.asm.tree.MethodNode
import java.io.File
import java.io.FileOutputStream
import java.util.jar.JarEntry
import java.util.jar.JarFile
import java.util.jar.JarOutputStream

fun Project.addStellarTasks() {
    if (!ext.relocation.isPresent) {
        ext.relocation.set(RelocationConfig())
    }

    val relocateTask = tasks.register("relocate") {
        group = STELLA_TASK_GROUP

        val originalJar = tasks.getByName("jar").outputs.files.singleFile

        doLast {
            val jar = JarFile(originalJar)
            val entries = jar.entries()
            val newJar = JarOutputStream(FileOutputStream(File(originalJar.parentFile, "${originalJar.nameWithoutExtension}-relocated.jar")))

            val classNameReplaceList = ext.relocation.get().classNameReplaceList
            val stringReplaceList = ext.relocation.get().stringReplaceList
            val packagePrefix = ext.packagePrefix.get()

            fun writeEntryToFile(file: JarFile, outStream: JarOutputStream, entry: JarEntry, entryName: String) {
                outStream.putNextEntry(JarEntry(entryName))
                outStream.write(file.getInputStream(entry).readBytes())
                outStream.closeEntry()
            }

            fun putEntry(
                entry: JarEntry,
                packagePrefix: String,
                jar: JarFile,
                classNameReplaceList: MutableMap<String, String>,
                stringReplaceList: MutableMap<String, String>,
                newJar: JarOutputStream
            ) {
                // only modify classes
                if (!entry.isDirectory) {
                    when {
                        entry.name.startsWith(packagePrefix.replace('.', '/')) -> {
                            val bytes = jar.getInputStream(entry).readBytes()

                            var cr = ClassReader(bytes)
                            var cw = ClassWriter(cr, 0)

                            cr.accept(ClassRemapper(cw, object : Remapper() {
                                override fun map(internalName: String): String =
                                    classNameReplaceList.entries.fold(internalName) { acc, (target, replacement) ->
                                        acc.replaceFirst(target, replacement)
                                    }
                            }), 0)

                            cr = ClassReader(cw.toByteArray())
                            val cn = ClassNode()
                            cr.accept(cn, 0)

                            for (insn in cn.methods.flatMap(MethodNode::instructions)) {
                                if (insn is LdcInsnNode) {
                                    if (insn.cst is String) {
                                        val cst = insn.cst as? String ?: continue
                                        if (stringReplaceList.containsKey(cst)) {
                                            val newCst = stringReplaceList[cst]
                                            if (newCst != null) {
                                                insn.cst = newCst
                                            }
                                        }
                                    }
                                }
                            }

                            cw = ClassWriter(cr, 0)
                            cn.accept(cw)

                            newJar.putNextEntry(JarEntry(classNameReplaceList.entries.fold(entry.name) { acc, (target, replacement) ->
                                acc.replaceFirst(target, replacement)
                            }))
                            newJar.write(cw.toByteArray())
                            newJar.closeEntry()
                        }

                        else -> {
                            if (!ext.relocation.get().extraRelocationAppliers.any { it(jar, newJar, entry, ::writeEntryToFile) }) {
                                writeEntryToFile(jar, newJar, entry, entry.name)
                            }
                        }
                    }
                }
            }

            while (entries.hasMoreElements()) {
                val entry = entries.nextElement()

                runCatching {
                    putEntry(entry, packagePrefix, jar, classNameReplaceList, stringReplaceList, newJar)
                }
            }

            for (file in configurations["compileClasspath"]) {
                val shadow = ext.relocation.get().relocationShadowJars.entries.find { (wantedJarName, _) -> file.name.contains(wantedJarName) } ?: continue

                val finalFile = shadow.value?.invoke(file) ?: file
                val shadowJar = JarFile(finalFile)
                val shadowEntries = shadowJar.entries()

                while (shadowEntries.hasMoreElements()) {
                    runCatching {
                        putEntry(shadowEntries.nextElement(), packagePrefix, shadowJar, classNameReplaceList, stringReplaceList, newJar)
                    }
                }
            }

            for (file in ext.relocation.get().relocationBundledJars) {
                val shadowJar = JarFile(file)
                val shadowEntries = shadowJar.entries()

                while (shadowEntries.hasMoreElements()) {
                    runCatching {
                        putEntry(shadowEntries.nextElement(), packagePrefix, shadowJar, classNameReplaceList, stringReplaceList, newJar)
                    }
                }
            }

            newJar.close()
        }

        outputs.file(originalJar.parentFile.resolve("${originalJar.nameWithoutExtension}-relocated.jar"))
    }

    arrayOf("publishLibraryPublicationToMavenRepository", "publishLibraryPublicationToMavenLocal").forEach {
        runCatching { tasks.getByName(it).dependsOn(relocateTask) }
    }

    tasks.getByName<Jar>("jar") {
        finalizedBy(tasks.getByName("relocate"))

        for (file in configurations["compileClasspath"]) {
            val shadow = ext.relocation.get().compileShadowJars.entries.find { (wantedJarName, _) -> file.name.contains(wantedJarName) } ?: continue

            val finalFile = shadow.value?.invoke(file) ?: file
            from(zipTree(finalFile)) {
                duplicatesStrategy = DuplicatesStrategy.EXCLUDE

                exclude("META-INF/*.SF")
                exclude("META-INF/*.DSA")
                exclude("META-INF/*.RSA")
            }
        }

        for (file in ext.relocation.get().compileBundledJars) {
            from(zipTree(file)) {
                duplicatesStrategy = DuplicatesStrategy.EXCLUDE

                exclude("META-INF/*.SF")
                exclude("META-INF/*.DSA")
                exclude("META-INF/*.RSA")
            }
        }
    }

    tasks.create("cleanBuild") {
        group = STELLA_TASK_GROUP

        dependsOn(tasks.getByName("clean"))
        finalizedBy(tasks.getByName("build"))
    }
}
