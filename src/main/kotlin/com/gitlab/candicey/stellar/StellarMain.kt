package com.gitlab.candicey.stellar

import com.gitlab.candicey.stellar.dependency.addStellarDependencies
import com.gitlab.candicey.stellar.task.addStellarTasks
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.artifacts.DependencyResolutionListener
import org.gradle.api.artifacts.ResolvableDependencies

class StellarMain : Plugin<Project> {
    override fun apply(proj: Project) {
        with(proj) {
            extensions.create("stellar", StellarExtension::class.java)

            afterEvaluate {
                if (!ext.packagePrefix.isPresent) {
                    ext.packagePrefix.set(group.toString())
                }

                addStellarDependencies()

                addStellarTasks()
            }
        }
    }
}