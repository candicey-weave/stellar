package com.gitlab.candicey.stellar

import com.gitlab.candicey.stellar.config.DependencyConfig
import com.gitlab.candicey.stellar.config.RelocationApplier
import com.gitlab.candicey.stellar.config.RelocationConfig
import com.gitlab.candicey.stellar.config.add
import com.gitlab.candicey.stellar.dependency.Dependency
import org.gradle.api.Project
import org.gradle.api.provider.Property
import org.objectweb.asm.ClassReader
import org.objectweb.asm.ClassWriter
import org.objectweb.asm.commons.ClassRemapper
import org.objectweb.asm.commons.Remapper
import org.objectweb.asm.tree.ClassNode
import org.objectweb.asm.tree.LdcInsnNode
import org.objectweb.asm.tree.MethodNode
import java.util.jar.JarEntry
import kotlin.properties.Delegates

interface StellarExtension {
    val packagePrefix: Property<String>
    val relocation: Property<RelocationConfig>
    val dependencies: Property<DependencyConfig>

    fun packagePrefix(prefix: String) = packagePrefix.set(prefix)

    fun relocate(block: RelocationConfigurationBuilder.() -> Unit) = relocation.set(build(RelocationConfigurationBuilder(), block).backing)

    fun dependencies(block: DependencyConfigurationBuilder.() -> Unit) = dependencies.set(build(DependencyConfigurationBuilder(), block).backing)
}

inline fun <C> build(builder: C, block: C.() -> Unit) = builder.also(block)

class RelocationConfigurationBuilder {
    @PublishedApi
    internal var backing = RelocationConfig()

    fun getConfig() = backing

    private fun <T> mutatingProperty(initial: T, mut: RelocationConfig.(T) -> RelocationConfig) =
        Delegates.observable(initial) { _, _, n -> backing = backing.mut(n) }

    fun relocateAsm() = addClassReplacement("org/objectweb/asm/", "net/weavemc/relocate/asm/")

    fun relocateMixin() = addClassReplacement("org/spongepowered/", "net/weavemc/relocate/spongepowered/")

    fun addShadowJar(jar: String) {
        backing.relocationShadowJars.add(jar)
    }

    fun addClassReplacement(from: String, to: String) {
        backing.classNameReplaceList[from] = to
    }

    fun addStringReplacement(from: String, to: String) {
        backing.stringReplaceList[from] = to
    }

    infix fun String.relocateTo(to: String) = addClassReplacement(this, to)

    infix fun String.replaceStringWith(to: String) = addStringReplacement(this, to)

}

class DependencyConfigurationBuilder {
    @PublishedApi
    internal var backing = DependencyConfig()

    fun getConfig() = backing

    private fun <T> mutatingProperty(initial: T, mut: DependencyConfig.(T) -> DependencyConfig) =
        Delegates.observable(initial) { _, _, n -> backing = backing.mut(n) }

    private fun createDependency(groupId: String, artifactId: String, repository: String, classifier: String? = null, extension: String = ".jar", addition: (Project.(thisDependency: Dependency) -> Unit)? = null): (String) -> Unit =
        { version: String -> backing.dependencies.add(Dependency(groupId, artifactId, version, repository, classifier, extension, addition)) }

    infix fun ((String) -> Unit).version(version: String) = this(version)
    infix fun ((String, String?, String?) -> Unit).version(version: String) = this(version, null, null)

    val zenithCore = createDependency("com.gitlab.candicey.zenithcore", "Zenith-Core", "https://gitlab.com/api/v4/projects/45235852/packages/maven") { thisDependency ->
        with(ext.relocation.get()) {
            classNameReplaceList["com/gitlab/candicey/zenithcore"] = "com/gitlab/candicey/zenithcore_v${thisDependency.version.replace('.', '_')}"
            stringReplaceList["@@ZENITH_CORE_VERSION@@"] = thisDependency.version
        }
    }

    val zenithCoreVersionedApi = { minecraftVersion: String,  ->
        val zenithCoreVersion = backing.dependencies.find { it.artifactId == "Zenith-Core" }?.version ?: error("Please add Zenith-Core dependency first")
        createDependency("com.gitlab.candicey.zenithcore.versioned", minecraftVersion, "https://gitlab.com/api/v4/projects/45235852/packages/maven")(zenithCoreVersion)
    }

    val zenithLoader = { version: String, relocateDestinationPackage: String?, versionsJsonConfigurationName: String? ->
        val dependency = createDependency("com.gitlab.candicey.zenithloader", "Zenith-Loader", "https://gitlab.com/api/v4/projects/50863327/packages/maven") {
            with(ext.relocation.get()) {
                classNameReplaceList["com/gitlab/candicey/zenithloader"] = "${(relocateDestinationPackage ?: ext.packagePrefix.get()).replace('.', '/')}/libs/zenithloader"

                relocationShadowJars.add("Zenith-Loader")

                extraRelocationAppliers.add(
                    buildRelocationApplier(
                        startPackage = "com/gitlab/candicey/zenithloader",
                        versionsJsonConfigurationName = versionsJsonConfigurationName
                    )
                )
            }
        }

        dependency(version)
    }

    val altera = { version: String, relocateDestinationPackage: String?, versionsJsonConfigurationName: String? ->
        val dependency = createDependency("com.gitlab.candicey.altera", "Altera", "https://gitlab.com/api/v4/projects/57337404/packages/maven") {
            with(ext.relocation.get()) {
                classNameReplaceList["com/gitlab/candicey/altera"] = "${(relocateDestinationPackage ?: ext.packagePrefix.get()).replace('.', '/')}/libs/altera"

                relocationShadowJars.add("Altera")

                extraRelocationAppliers.add(
                    buildRelocationApplier(
                        startPackage = "com/gitlab/candicey/altera",
                        versionsJsonConfigurationName = versionsJsonConfigurationName
                    )
                )
            }
        }

        dependency(version)
    }

    val concentra = createDependency("com.gitlab.candicey.concentra", "Concentra", "https://gitlab.com/api/v4/projects/58199401/packages/maven") { thisDependency ->
        with(ext.relocation.get()) {
            stringReplaceList["@@CONCENTRA_VERSION@@"] = thisDependency.version
        }
    }

    fun Project.buildRelocationApplier(
        startPackage: String,
        versionsJsonConfigurationName: String?
    ): RelocationApplier = { originalJar, newJar, entry, writeEntryToFile ->
        with(ext.relocation.get()) {
            when {
                entry.name.startsWith(startPackage) -> {
                    val bytes = originalJar.getInputStream(entry).readBytes()

                    var cr = ClassReader(bytes)
                    var cw = ClassWriter(cr, 0)

                    cr.accept(ClassRemapper(cw, object : Remapper() {
                        override fun map(internalName: String): String =
                            classNameReplaceList.entries.fold(internalName) { acc, (target, replacement) ->
                                acc.replaceFirst(target, replacement)
                            }
                    }), 0)

                    cr = ClassReader(cw.toByteArray())
                    val cn = ClassNode()
                    cr.accept(cn, 0)

                    for (insn in cn.methods.flatMap(MethodNode::instructions)) {
                        if (insn is LdcInsnNode) {
                            if (insn.cst is String) {
                                val cst = insn.cst as? String ?: continue
                                if (stringReplaceList.containsKey(cst)) {
                                    val newCst = stringReplaceList[cst]
                                    if (newCst != null) {
                                        insn.cst = newCst
                                    }
                                }
                            }
                        }
                    }

                    cw = ClassWriter(cr, 0)
                    cn.accept(cw)

                    newJar.putNextEntry(JarEntry(classNameReplaceList.entries.fold(entry.name) { acc, (target, replacement) ->
                        acc.replaceFirst(target, replacement)
                    }))
                    newJar.write(cw.toByteArray())
                    newJar.closeEntry()

                    true
                }

                entry.name == "zenithloader/dependencies/${versionsJsonConfigurationName ?: ext.packagePrefix.get().substringAfterLast('.')}.versions.json" -> {
                    val bytes = originalJar.getInputStream(entry).readBytes()
                    var string = String(bytes)

                    for ((target, replacement) in stringReplaceList) {
                        string = string.replace(target, replacement)
                    }

                    newJar.putNextEntry(JarEntry(entry.name))
                    newJar.write(string.toByteArray())
                    newJar.closeEntry()

                    true
                }

                else -> false
            }
        }
    }
}