package com.gitlab.candicey.stellar

import org.gradle.api.Project
import org.gradle.kotlin.dsl.getByType

val Project.ext: StellarExtension
    get() = extensions.getByType()