package com.gitlab.candicey.stellar.dependency

import com.gitlab.candicey.stellar.STELLAR_DEPENDENCIES_DIRECTORY
import com.gitlab.candicey.stellar.config.DependencyConfig
import com.gitlab.candicey.stellar.ext
import org.gradle.api.Project
import java.net.URL
import kotlin.io.path.Path
import kotlin.io.path.createDirectories
import kotlin.io.path.exists
import kotlin.io.path.writeBytes

data class Dependency(
    val groupId: String,
    val artifactId: String,
    val version: String,
    val repository: String,
    val classifier: String? = null,
    val extension: String = ".jar",
    val addition: (Project.(thisDependency: Dependency) -> Unit)? = null,
) {
    val artifactUrl by lazy { "$repository/${groupId.replace(".", "/")}/$artifactId/$version/$artifactId-$version${classifier?.let { "-$it" } ?: ""}$extension" }

    val path by lazy { Path(STELLAR_DEPENDENCIES_DIRECTORY, groupId.replace(".", "/"), artifactId, version, "$artifactId-$version${classifier?.let { "-$it" } ?: ""}$extension") }

    val exists get() = path.exists()

    fun download() = with(path) {
        try {
            parent?.createDirectories()
            writeBytes(URL(artifactUrl).readBytes())

            null
        } catch (e: Throwable) {
            e
        }
    }
}

fun Project.addStellarDependencies() {
    if (!ext.dependencies.isPresent) {
        ext.dependencies.set(DependencyConfig())
    }

    for (dependency in ext.dependencies.get().dependencies) {
        if (!dependency.exists) {
            dependency.download()?.let {
                error("Failed to download dependency: $dependency\n$it")
            }
        }

        dependencies.add("compileOnly", project.files(dependency.path))

        dependency.addition?.invoke(this, dependency)
    }
}