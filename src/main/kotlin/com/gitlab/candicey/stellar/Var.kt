package com.gitlab.candicey.stellar

const val STELLA_TASK_GROUP = "stellar"

val HOME by lazy { System.getProperty("user.home") }
val WEAVE_DIRECTORY by lazy { "$HOME/.weave" }
val STELLAR_DIRECTORY by lazy { "$WEAVE_DIRECTORY/Stellar" }
val STELLAR_DEPENDENCIES_DIRECTORY by lazy { "$STELLAR_DIRECTORY/dependencies" }